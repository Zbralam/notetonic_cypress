// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './/commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

class Home {


    navigate() {
      //cy.wait(5000)
      cy.visit('https://notetonic-onenote-staging.azurewebsites.net/')

      
     
    }
  
    verifyCreateAccountText () {
     cy.get('.button-row > .is-info > span').contains('Create an account')
  
    }
    
    verifyLoginBackText() { 
        cy.get('.is-primary > span').contains('Log back in')
    }
  
    clickLoginButton() { 
        cy.get('.is-primary > span').click()
    }
  
    clickOnLoginButton () {
      cy.get('.button-row > .is-primary').click()
    }
  
  
  }

  export default Home;

